
import json

def abrir_archivo():
    datos_estudiante = {}
    #Lectura del fichero json con permiso de lectura
    with open('estudiante.json') as archivo:
        #Cargar la información del archivo json
        datos_estudiante = json.load(archivo)

    return datos_estudiante

def guardar_archivo(datos_estudiante: dict):
    #Abrir fichero con permisos de escritura
    with open('estudiante.json', 'w') as archivo:
        #Sobreescribir la información del archivo json
        json.dump(datos_estudiante, archivo)
        print('Información guardada con éxito')


estudiante = abrir_archivo()
estudiante['edad'] = 28
guardar_archivo(estudiante)
