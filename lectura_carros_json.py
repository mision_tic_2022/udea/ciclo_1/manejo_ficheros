import json

def cargar_datos():
    lista_carros = []
    with open('carros.json') as archivo:
        lista_carros = json.load(archivo)
        print('¡Datos cargados con éxito!')
    return lista_carros

def guardar_datos(lista_carros: list):
    with open('carros.json', 'w') as archivo:
        json.dump(lista_carros, archivo)
        print('¡Datos guardados con éxito!')

def mostrar_datos(lista_carros: list):
    contador = 1
    for carro in lista_carros:
        propietario = carro['propietario']
        placa = carro['placa']
        telefono = carro['telefono_propietario']
        print(f'----------------Vehículo {contador}----------------')
        print(f'Propietario: {propietario}')
        print(f'Placa: {placa}')
        print(f'Teléfono: {telefono}')
        print('-----------------------------------------------')
        contador += 1

carros: list = cargar_datos()
mostrar_datos(carros)
nuevo_carro = {
    'placa': 'ABC123',
    'propietario': 'Santiago Torres',
    'telefono_propietario': '987654'
}
carros.append(nuevo_carro)
print('----------------------')
mostrar_datos(carros)
guardar_datos(carros)

'''
------------Vehículo 1------------
Propietario: 
Placa: --
Teléfono:
'''