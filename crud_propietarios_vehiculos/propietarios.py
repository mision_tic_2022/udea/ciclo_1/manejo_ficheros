import json 

def cargar_datos():
    datos: list = []
    try:
        with open('crud_propietarios_vehiculos/datos.json') as archivo:
            datos = json.load(archivo)
    except:
        datos = []
    return datos

def guardar_datos(datos: list):
    with open('crud_propietarios_vehiculos/datos.json', 'w') as archivo:
        json.dump(datos, archivo)
        print('¡Datos guardados con éxito!')

def registrar_propietario(informacion: list):
    print('-------------REGISTRAR PROPIETARIO--------------')
    #Crear diccionario y solicitar datos al usuario
    propietario: dict = {
        'nombre': input('Nombre: '),
        'apellido': input('Apellido: '),
        'celular': input('Celular: '),
        'vehiculos': []
    }
    #Añadir propietario a la lista 'información'
    informacion.append(propietario)
    #Actualizar fichero json
    guardar_datos(informacion)

def registrar_vehiculo(informacion: list):
    apellido = input('Apellido: ')
    propietario = list( filter( lambda e: e['apellido']==apellido ,informacion ) )
    if len(propietario) > 0:
        placa = input('Placa del vehículo: ')
        vehiculos:list = propietario[0]['vehiculos']
        vehiculos.append(placa)
        guardar_datos(informacion)
    else:
        print('\nEl propietario no se encuentra registrado\n')

def menu():
    informacion: list = cargar_datos()
    opcion = 0
    while opcion != 7:
        mensaje_menu = '----------------CRUD CON PERSISTENCIA DE DATOS (UTILIZANDO JSON)--------------------\n'
        mensaje_menu += '1) Registrar propietario\n'
        mensaje_menu += '2) Registrar vehículo\n'
        mensaje_menu += '3) Actualizar info propietario\n'
        mensaje_menu += '4) Eliminar propietario\n'
        mensaje_menu += '5) Eliminar vehículo\n'
        mensaje_menu += '6) Eliminar todos los datos de la BD\n'
        mensaje_menu += '7) Salir\n'
        mensaje_menu += '>>> '
        #Solicitar una opción al usuario
        opcion = int( input(mensaje_menu) )
        #Evaluar la opción ingresada por el usuario
        if opcion == 1:
            registrar_propietario(informacion)
        elif opcion == 2:
            pass
        elif opcion == 3:
            pass
        elif opcion == 4:
            pass
        elif opcion == 5:
            pass
        elif opcion == 6:
            pass
        elif opcion != 7:
            print('\nIngrese una opción válida\n')

menu()
